<?php

	/**
     * Display/Set a listing of the resource (Card).
     *
     * @author       Faisal Iqbal <faisal.iqbal.085@gmail.com>
     */

	namespace src;

	/**
 	 * This class contains business login for the class Card.
 	*/

	class Card {

		/** @var string Should contain from */	
		protected $from = '';
		/** @var string Should contain to */
		protected $to = '';
		/** @var string Should contain Transportation Type */
		protected $transportationType = '';
		/** @var string|null Should contain text */
		protected $text = ''; 
		/** @var string|null Should contain list of options */
		protected $options = []; 

		/**
		 * Constructor used to set all the properties of the class.
		 *
		 * @param string $from
		 * @param string $to
		 * @param string $transportationType
		 * @param array $options
		 * @param string $text
		 */
		public function __construct($from, $to, $transportationType, $options = [], $text = '') {
			$this->from = $from;
			$this->to = $to;
			$this->transportationType = $transportationType;
			$this->options = $options;
			$this->text = $text;
		}

		/**
		 * Get Journey From
		 * @return string
		 */
		public function getFrom() {
			return $this->from;
		}

		/**
		 * Set from property
		 * @param string $from
		 */
		public function setFrom($from) {
			$this->from = $from;
		}

		/**
		 * Get Journey To
		 * @return string
		 */
		public function getTo() {
			return $this->to;
		}

		/**
		 * Set To property
		 * @param string $to
		 */
		public function setTo($to) {
			$this->to = $to;
		}

		/**
		 * Get Transportation Type
		 * @return string
		 */
		public function getTransportationType() {
			return $this->transportationType;
		}

		/**
		 * Set Transportation Type
		 * @param string $transportationType
		 */
		public function setTransportationType($transportationType) {
			$this->transportationType = $transportationType;
		}

		/**
		 * Get Text value.
		 * @return string
		 */
		public function getText() {
			return $this->text;
		}

		/**
		 * Set Text value.
		 * @param string $text
		 */
		public function setText($text) {
			$this->text = $text;
		}

		/**
		 * Get list of options
		 * @return array
		 */
		public function getOptions() {
			return $this->options;
		}

		/**
		 * Set list of option.
		 * @param array $options
		 */
		public function setOptions($options) {
			$this->options = $options;
		}

	}