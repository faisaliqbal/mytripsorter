<?php

	/**
     * Transportation Type
     *
     * @author       Faisal Iqbal <faisal.iqbal.085@gmail.com>
     */
	namespace src\TransportationTypes;

	/**
 	 * Defining Transportation Type Interface
 	*/

	interface TransportationTypeInterface{

		/**
		 * refine card instructions
		 *
		 * @param Card $card
		 * @return Card $card
		 */
		public function refineCardInstructions($card);
	}