<?php

	/**
     * Display a listing of the resource.
     *
     * @author       Faisal Iqbal <faisal.iqbal.085@gmail.com>
     */

	namespace src\TransportationTypes;

	use src\Card;

	/**
 	 * This class contains business login for the Train Transportation Type
 	*/

	class Train implements TransportationTypeInterface {

		/**
		 * refine Card Instructions
		 *
		 * @param Card $card
		 * @return Card $card
		 */
		public function refineCardInstructions($card) {
			
			$options = $card->getOptions();
			$text = 'Take train ';

			if (isset($options['trainNum'])) {
				$text .= sprintf("with number %s", $options['trainNum']);
			}

			$text .= sprintf(" from %s to %s", $card->getFrom() , $card->getTo());

			if (isset($options['seat'])) {
				$text .= sprintf(", seat %s", $options['seat']);
			}

			if (isset($options['comment'])) {
				$text .= sprintf(", %s", $options['comment']);
			}

			if (substr($text, -1) != '.') {
				$text .= '.';
			}

			$card->setText($text);
			return $card;
		}
	}