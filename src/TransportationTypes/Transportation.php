<?php

	/**
     * Display a listing of the resource.
     *
     * @author       Faisal Iqbal <faisal.iqbal.085@gmail.com>
     */

	namespace src\TransportationTypes;

	use src\Journey;
	
	/**
 	 * This class contains business login for the Transportation
 	*/

	class Transportation {

		/**
		 * makeType
		 * Factory method for any Transportation classes
		 *
		 * @param string $type  Should contain type
		 * @return null|Train
		 */

		private function makeType($type) {

			$class_name = ucfirst($type);

			if(!class_exists($class_name)) {
				return;
			}
			
			return new $class_name();
		}

		/**
		 * Refine Journey Instructions
		 *
		 * @param Journey $journey
		 * @return Journey $journey
		 */
		public function refineJourneyInstructions($journey) {

			$cards = $journey->getCards();
			$refinedCards = [];

			foreach ($cards as $card) {
				$type = $card->getTransportationType();
				$transportation = $this->makeType("\src\TransportationTypes\\".$type);
				if (method_exists($transportation, 'refineCardInstructions')) {
					$refinedCards[]  = $transportation->refineCardInstructions($card);
				}
			}

			$journey->setCards($refinedCards);
			return $journey;
		}
	}