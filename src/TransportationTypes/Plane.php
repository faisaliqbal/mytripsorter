<?php
	
	/**
     * Display a listing of the resource.
     *
     * @author       Faisal Iqbal <faisal.iqbal.085@gmail.com>
     */

	namespace src\TransportationTypes;

	use src\Card;

	/**
 	 * This class contains business login for the Plane Transportation Type
 	*/

	class Plane implements TransportationTypeInterface {

		/**
		 * refine card instructions
		 *
		 * @param Card $card
		 * @return Card $card
		 */
		public function refineCardInstructions($card) {
			
			$options = $card->getOptions();
			$text = 'Take flight ';

			if (isset($options['flight'])) {
				$text .= sprintf("number %s", $options['flight']);
			}

			$text .= sprintf(" from %s to %s", $card->getFrom() , $card->getTo());

			if (isset($options['gate'])) {
				$text .= sprintf(", gate %s", $options['gate']);
			}

			if (isset($options['seat'])) {
				$text .= sprintf(", seat %s", $options['seat']);
			}

			if (isset($options['comment'])) {
				$text .= sprintf(", %s", $options['comment']);
			}

			if (substr($text, -1) != '.') {
				$text .= '.';
			}

			$card->setText($text);

			return $card;
		}
	}