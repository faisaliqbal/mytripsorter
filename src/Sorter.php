<?php
	
	/**
     * Display a listing of the resource.
     *
     * @author       Faisal Iqbal <faisal.iqbal.085@gmail.com>
     */

	namespace src;

	use src\Journey;

	/**
 	 * This class is used to sort unorder list of cards.
 	*/

	class Sorter{

		/**
    	 * Sorted Cards
    	 * @var array
    	*/

		protected $sortedCards = [];

		/**
		 * Sort by recursiveSorting and initialize $sortedCards property as we go
		 * @param Journey $journey
		 * @return Journey $journey
		 */
		public function sort($journey) {
			
			$cards = $journey->getCards();

			$this->recursiveSort($cards);

			$journey->setCards($this->getSortedCards());

			return $journey;
		}


		/**
		 * recursive Sort
		 *
		 * @param Card[] $cards
		 * @return Card[] $sortedCards
		 */
		private function recursiveSort($cards) {

			// if last card, add it to global sortedCards and stop the recursion.
			if (count($cards) == 1) {
				
				$this->addSortedCard($cards[0]);
				return;
			}

			foreach ($cards as $key => $card) {

				// Get the first card, add it to global sortedCards
				// Then unset and reindex cards and continue recursion
				
				if($this->isFirst($card, $cards)) {
					
					$this->addSortedCard($card);
					unset($cards[$key]);
					$cards = array_values($cards);
					
					return $this->recursiveSort($cards);
				}
			}
		}

		/**
		 * The card is first if it is from a location that is never been a "to destination" in other cards
		 * @param Card $checkingCard
		 * @param Card [] $cards
		 * @return bool
		 */
		private function isFirst($checkingCard, $cards) {
			foreach ($cards as $card) {
				if (strtolower($checkingCard->getFrom()) == strtolower($card->getTo())) {

					return false;
				}
			}

			return true;
		}

		/**
		 * Get Sorted Cards
		 *
		 * @return Card[]
		 */
		public function getSortedCards() {
			
			return $this->sortedCards;
		}


		/**
		 * Add Sorted card
		 *
		 * @param Card $card
		 */
		public function addSortedCard($card) {
			
			array_push($this->sortedCards, $card);
		
		}

	}