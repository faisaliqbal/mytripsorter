<?php
	
	/**
     * Display/Set a listing of the resource (Card).
     *
     * @author       Faisal Iqbal <faisal.iqbal.085@gmail.com>
     */

	namespace src;

	/**
 	 * This class contains business login for the class Journey.
 	*/

	class Journey {
		
		/**
    	 * cards
    	 * @var array
    	*/

		protected $cards = [];

		/**
		 * Getting list of cards.
		 * @return Card[]
		 */
		public function getCards() {
			return $this->cards;
		}

		/**
		 * Setting list of cards.
		 * @param Card[] $cards
		 */
		public function setCards($cards) {
			$this->cards = $cards;
		}

		/**
		 * Adding Card in the Stack.
		 *
		 * @param Card $card
		 */
		public function addCard($card){
			array_push($this->cards, $card);
		}

	}