<?php
	require_once(__DIR__.'/src/Card.php');
	require_once(__DIR__.'/src/Journey.php');
	require_once(__DIR__.'/src/Sorter.php');
	require_once(__DIR__.'/src/TransportationTypes/TransportationTypeInterface.php');
	require_once(__DIR__.'/src/TransportationTypes/Train.php');
	require_once(__DIR__.'/src/TransportationTypes/Plane.php');
	require_once(__DIR__.'/src/TransportationTypes/Bus.php');
	require_once(__DIR__.'/src/TransportationTypes/Transportation.php');
	
