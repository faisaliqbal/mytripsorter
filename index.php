<?php
	use src\Card;
	use src\Journey;
	use src\Sorter;
	use src\TransportationTypes\Transportation;

	require_once(__DIR__.'/includes.php');

	$journey = new Journey();

	// Adding list of unordered cards
	
	$options = ['comment' => 'No seat assignment.'];
	$card = new Card('Barcelona','Gerona Airport','Bus', $options);
	$journey->addCard($card);

	$options = ['flight' => 'SK22','gate' => '22', 'seat' =>'7B','comment'=>'Baggage will we automatically transferred from your last leg.'];
	$card = new Card('Stockholm','New York JFK','Plane',$options);
	$journey->addCard($card);

	$options = ['trainNum' => '78A','seat' => '45B'];
	$card = new Card('Madrid','Barcelona','Train',$options);
	$journey->addCard($card);

	$options = ['flight' => 'SK455','gate' => '45B', 'seat' =>'3A', 'comment'=>'Baggage drop at ticket counter 344.'];
	$card = new Card('Gerona Airport','Stockholm','Plane',$options);
	$journey->addCard($card);

	$transportation = new Transportation();

	$journey = $transportation->refineJourneyInstructions($journey);

	$sorter = new Sorter();
	$journey = $sorter->sort($journey);
?>

<?php foreach ($journey->getCards() as $card): ?>
	<?php echo $card->getText(); ?> <br>
<?php endforeach ?>