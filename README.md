
## Note:

Only Composer, PHPUnit, PHPdoc included.

## How to Execute the code:

	// create a new Journey
	$journey = new Journey();

	// Add cards to journey using the format Card('From','To','TransportationMethod (Train, Bus, Plane)', options[]) .

	// Adding list of unordered cards
	
	$options = ['comment' => 'No seat assignment.'];
	$card = new Card('Barcelona','Gerona Airport','BUS', $options);
	$journey->addCard($card);

	$options = ['flight' => 'SK22','gate' => '22', 'seat' =>'7B','comment'=>'Baggage will we automatically transferred from your last leg.'];
	$card = new Card('Stockholm','New York JFK','Plane',$options);
	$journey->addCard($card);

	$options = ['trainNum' => '78A','seat' => '45B'];
	$card = new Card('Madrid','Barcelona','Train',$options);
	$journey->addCard($card);

	$options = ['flight' => 'SK455','gate' => '45B', 'seat' =>'3A', 'comment'=>'Baggage drop at ticket counter 344.'];
	$card = new Card('Gerona Airport','Stockholm','Plane',$options);
	$journey->addCard($card);

	// Now a new Transportation to refine the journey instructions.
    $transportation = new Transportation();
    $journey = $transportation->refineJourneyInstructions($journey);
    	    
	// Sorting the Journey Cards.
	    $sorter = new Sorter();
        $journey = $sorter->sort($journey);
    // See the result
        <?php foreach ($journey->getCards() as $card): ?>
        	<?php echo $card->getText(); ?> <br>
        <?php endforeach ?>

## How to Test the code:

- All the tests are in /Tests Folder
- Command: phpunit Tests

## How to Extend the code for new transportation types:

- To create new transportation type, you have to add a new class in the transportationTypes folder that will implement transportationTypeInterface.
For example, if you want to add new transportation type "Train", you will need to implement implementing refineCardInstructions() Method with the code something like.

refineCardInstructions($card) {
	$options = $card->getOptions();
	$text = '';
	$text.='Take train '; 

	if (isset($options['trainNum'])) {
		$text .= sprintf("with number %s", $options['trainNum']);
	}

	$text .= sprintf(" from %s to %s", $card->getFrom() , $card->getTo());

	if (isset($options['seat'])) {
		$text .= sprintf(", seat %s", $options['seat']);
	}

	if (isset($options['comment'])) {
		$text .= sprintf(", %s", $options['comment']);
	}

	if (substr($text, -1) != '.') {
		$text .= '.';
	}

	$card->setText($text);
	return $card;
}