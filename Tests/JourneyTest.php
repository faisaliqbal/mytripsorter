<?php
	namespace Tests;

	use PHPUnit_Framework_TestCase;
	use src\Card;
	use src\Journey;

	require_once(__DIR__.'/../includes.php');

	class JourneyTest extends PHPUnit_Framework_TestCase {

		function testJourneyCanAddCards() {

			$journey = new Journey();

			$options = ['trainNum' => '78A','seat'	   => '45B'];
			$card = new Card('Madrid','Barcelona','Train','',$options);

			$journey->addCard($card);

			$card = new Card('Barcelona','Gerona Airport','BUS');

			$journey->addCard($card);

			$this->assertEquals(2, count($journey->getCards()));

		}
	}
