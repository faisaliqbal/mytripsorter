<?php
	namespace Tests;

	use PHPUnit_Framework_TestCase;
	use src\Card;

	require_once(__DIR__.'/../includes.php');

	class CardTest extends PHPUnit_Framework_TestCase {
		
		function testCardConstructor() {

			$options = ['trainNum' => '78A','seat'=> '45B'];
			$card = new Card('Madrid','Barcelona','Train',$options);

			$this->assertEquals('Madrid',$card->getFrom());
			$this->assertEquals('Barcelona',$card->getTo());
			$this->assertEquals('Train',$card->getTransportationType());
			$this->assertEquals($options,$card->getOptions());
		}
	}