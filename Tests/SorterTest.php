<?php
	namespace Tests;

	use PHPUnit_Framework_TestCase;
	use src\Sorter;
	use src\Card;
	use src\Journey;

	require_once(__DIR__.'/../includes.php');

	class SorterTest extends PHPUnit_Framework_TestCase {

		function testSortingAlgorithm() {

			$journey = new Journey();

			$card = new Card('Barcelona','Gerona Airport','BUS');
			$journey->addCard($card);

			$options = ['flight' => 'SK22','gate' => '22', 'seat' =>'7B','comment'=>'Baggage will we automatically transferred from your last leg.'];
			$card = new Card('Stockholm','New York','Plane',$options);
			$journey->addCard($card);

			$options = ['trainNum' => '78A','seat' => '45B'];
			$card = new Card('Madrid','Barcelona','Train',$options);
			$journey->addCard($card);

			$options = ['flight' => 'SK455','gate' => '45B', 'seat' =>'3A', 'comment'=>'Baggage drop at ticket counter 344.'];
			$card = new Card('Gerona Airport','Stockholm','Plane',$options);
			$journey->addCard($card);

			$sorter = new Sorter();
			$sortedJourney = $sorter->sort($journey);
			$cards = $sortedJourney->getCards();

			$this->assertEquals('Madrid', $cards[0]->getFrom());
			$this->assertEquals('Barcelona', $cards[1]->getFrom());
			$this->assertEquals('Gerona Airport', $cards[2]->getFrom());
			$this->assertEquals('Stockholm', $cards[3]->getFrom());

		}
	}
