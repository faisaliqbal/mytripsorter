<?php
	namespace Tests;

	use PHPUnit_Framework_TestCase;
	use src\Card;
	use src\Journey;
	use src\TransportationTypes\Transportation;

	require_once(__DIR__.'/../includes.php');

	class TransportationTest extends PHPUnit_Framework_TestCase {

		function testCardConstructor() {
			
			$journey = new Journey();
			$options = ['trainNum' => '78A','seat'=> '45B'];
			$card = new Card('Madrid','Barcelona','Train',$options);
			$journey->addCard($card);

			$transportation = new Transportation();
			$refinedJourney = $transportation->refineJourneyInstructions($journey);

			foreach ($refinedJourney->getCards() as $card) {
				$this->assertContains('train', strtolower($card->getText()));
				$this->assertContains('seat 45b', strtolower($card->getText()));
				$this->assertContains('number 78a', strtolower($card->getText()));
			}

		}
	}
